import Fighter from "./fighter.js";

class Fight {
	fighters = new Map();
	names = []; 

	addFighter(fighter) {
		let {id: id, name: name, health: health, attack: attack,
			defense: defense} = fighter;
		this.fighters.set(name, new Fighter(name, health, attack, defense));
		this.names.push(name);
		if (this.fighters.size == 2) {
			this.clearStage();
		} 
	}

	updateView(ob, damage) {
		let fighterViews = document.querySelectorAll(".fighter");
		for (let v of fighterViews) {
			let name = v.querySelector(".name").innerText;
			v.querySelector("#fhealth").innerText =
				ob.fighters.get(name).health;
			if (ob.names[0] == name) {
				if (damage > 0)
					v.setAttribute("style",
					"box-shadow: inset 0 0 60px -20px #db5555");
				else
					v.setAttribute("style",
					"box-shadow: inset 0 0 60px -20px #54aaaa");
			}
		}
	}

	announceWinner() {
		let winner = null;
		for (let fighter of this.fighters) {
			if (!winner || fighter[1].health > winner[1].health) {
				winner = fighter;
			}
		}
		for (let v of document.querySelectorAll(".fighter")) {
			v.setAttribute("style", "box-shadow: none");
			if (v.querySelector(".name").innerText != winner[1].name) {
					document.querySelector(".fighters").removeChild(v);
			};
		}
	}

	strike(ob) {
		for (let v of document.querySelectorAll(".fighter")) {
			v.setAttribute("style", "box-shadow: none");
		}
		let fighter1 = ob.fighters.get(ob.names[1]);
		let fighter0 = ob.fighters.get(ob.names[0]);
		let damage = fighter1.getHitPower() - fighter0.getBlockPower();
		if (damage > 0) {
			let newHealth = Number((fighter0.health - damage).toFixed(1));
			fighter0.health = newHealth > 0 ? newHealth : 0;
		}
		ob.updateView(ob, damage);
		this.names.reverse();
	}

	startFight() {
		let ob = this;
		document.querySelector("button").setAttribute("hidden", "");
		let interv = setInterval(function() {
			ob.strike(ob);
			for (let [key, fighter] of ob.fighters) {
				if (fighter.health <= 0)
				{
					clearInterval(interv);
					ob.announceWinner();                 
				}
			}
		}, 200);
	}

	clearStage() {
		let els = document.querySelectorAll(".fighter");
		for (let el of els) {
			if (!this.fighters.has(el.innerText)
			&& !this.fighters.has(el.innerText)) {
				document.querySelector(".fighters").removeChild(el);
			}
		}
		document.querySelector(".fighters")
			.querySelector(".fighter:last-of-type img")
			.setAttribute("style", "transform: scaleX(-1)");
		let b = document.createElement('button');
		b.innerHTML = "fight";
		document.querySelector('.fighters').after(b);
		document.querySelector("button")
			.addEventListener("click", ()=>this.startFight(), false);
		let a = document.querySelectorAll(".fighter");
		for (let i = 0; i < this.fighters.size; i++) {
			let name = a[i].innerText;
			let b = document.createElement('div');
			b.setAttribute("style", "display: flex; justify-content: space-between");
			b.innerHTML =
				`<p>Health:</p><p id="fhealth">${this.fighters.get(name).health}</p>`;
			a[i].appendChild(b);
			a[i].setAttribute("style", "pointer-events:none");
		}
	}
}
export default Fight;