class Fighter {
    constructor(name, health, attack, defense) {
        this.name = name;
        this.defense = defense;
        this.health = health;
        this.attack = attack;
    }

    getHitPower() {
        let criticalHitChance = Number((Math.random() + 1).toFixed(1));
        let hitPower = Number((this.attack * criticalHitChance).toFixed(1));
        return hitPower;
    }

    getBlockPower() {
        let dodgeChance = Number((Math.random() + 1).toFixed(1));
        let blockPower = Number((this.defense * dodgeChance).toFixed(1));
        return blockPower;
    }
}

export default Fighter;