import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fight from './fight.js';

class FightersView extends View {
  root = document.querySelector("#root");
  modal = document.createElement('div');
  fightersDetailsMap = new Map();

  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

    this.fight = new Fight();
    this.modal.className = "modal";
    this.modal.innerHTML = '<a id="close" href="#">&times;</a>\
      <div class="content"><p id="name">Fighter</p>\
      <label for="health">Health</label><input id="health" type="text">\
      <label for="health">Attack</label><input id="attack" type="text">\
      <label for="health">Defense</label><input id="defense" type="text"></div>\
      <a id="confirm" href="#">&#10004;</a>';
    this.modal.querySelector("#close").addEventListener("click",
      ()=>this.root.removeChild(this.modal));
    this.modal.querySelector("#confirm").addEventListener("click", ()=>
      {
        let info = ["health", "attack", "defense"];
        for (let i of info) {
          this.current[`${i}`] = Math.abs(
            parseInt(this.modal.querySelector(`#${i}`).value));
        }
        this.root.removeChild(this.modal);
        this.fight.addFighter(this.current);
      }
    );
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }
  
  displayInfo(fighter) {
    let info = ["health", "attack", "defense", "name"];
    for (let i of info) {
      this.modal.querySelector(`#${i}`).value = fighter[`${i}`];
    }
  }

  handleFighterClick(event, fighter) {
    this.current = fighter;
    if (!document.body.contains(this.modal)) {
      this.root.appendChild(this.modal);
    }
    if ("health" in fighter && "attack" in fighter && "defense" in fighter) {
      this.fightersDetailsMap.set(fighter._id, fighter);
      this.displayInfo(fighter);
    }
    else {
      const f = fighterService.getFighterDetails(fighter._id)
      .then((res)=>{
        fighter["health"] = res["health"];
        fighter["attack"] = res["attack"];
        fighter["defense"] = res["defense"];
        this.current = fighter;
        this.fightersDetailsMap.set(fighter._id, res);
        this.displayInfo(fighter);
      });
    }
  }
}

export default FightersView;